import pandas as pd
import math

def get_titatic_dataframe() -> pd.DataFrame:
    df = pd.read_csv("train.csv")
    return df


def get_filled():
    df = get_titatic_dataframe()

    def extract_title(name):
        if 'Mr.' in name:
            return 'Mr.'
        elif 'Mrs.' in name:
            return 'Mrs.'
        elif 'Miss.' in name:
            return 'Miss.'
        else:
            return 'Other'
    
    df['Title'] = df['Name'].apply(extract_title)
    
    median_ages = df.groupby('Title')['Age'].median().round().astype(int)
    
    missing_values = df[df['Age'].isnull()].groupby('Title')['Age'].size()
    
    results = []
    
    titles = ['Mr.', 'Mrs.', 'Miss.']
    
    for title in titles:
        median_age = median_ages.get(title, 0)
        missing_count = missing_values.get(title, 0)
        
        df.loc[(df['Age'].isnull()) & (df['Title'] == title), 'Age'] = median_age
        
        results.append((title, missing_count, median_age))
    
    return results